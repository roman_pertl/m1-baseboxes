# M1 Baseboxes

M1 Baseboxes contains packer templates for M1 (Apple Silicon) based Macs.  

Chef Software's Bento packer templates provided the initial inspiration.  The main work done here was converting the json files to the HCL2 template format.  For some reason, the Parallels SDK is not recognized when a json files is used, but it works fine on the M1 when a HCL2 template is used. Beyond the conversion, other scripts were removed that are not relevant to the M1 platform.  For example, Parallels is currently the only provider, so Virtualbox scripts were removed.  

Included are templates for Ubuntu and Debian including Ubuntu 22.04.

The Vagrant public Box repository has all the templates hosted if you do not wish to build them yourself.

## Requirements

- Packer
- Vagrant
- Parallels
- Parallels SDK

## Build Example

The following command sequence will build the Ubuntu 22.04 base box:

```
cd ubuntu
packer init ubuntu-22.04-arm64.pkr.hcl
packer build ubuntu-22.04-arm64.pkr.hcl
```

