packer {
  required_plugins {
    parallels = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/parallels"
    }
  }
}

variables {
  basebox_name     = "ubuntu-22.04-arm64"
  build_directory  = "../builds"
  iso_name         = "ubuntu-22.04.1-live-server-arm64.iso"
  iso_checksum     = "bc5a8015651c6f8699ab262d333375d3930b824f03d14ae51e551d89d9bb571c"
  mirror           = "http://cdimage.ubuntu.com"
  mirror_directory = "releases/22.04/release"
}

locals {
  http_directory = "${path.root}/http"
}

source "parallels-iso" "ubuntu-22-04-arm64" {
  boot_command = [
    " <wait>",
    " <wait>",
    " <wait>",
    " <wait>",
    " <wait>",
    "c",
    "<wait>",
    "set gfxpayload=keep",
    "<enter><wait>",
    "linux /casper/vmlinuz quiet<wait>",
    " autoinstall<wait>",
    " ds=nocloud-net<wait>",
    "\\;s=http://<wait>",
    "{{ .HTTPIP }}<wait>",
    ":{{ .HTTPPort }}/<wait>",
    " ---",
    "<enter><wait>",
    "initrd /casper/initrd<wait>",
    "<enter><wait>",
    "boot<enter><wait>"
  ]
  boot_wait              = "5s"
  cpus                   = "2"
  disk_size              = "65536"
  guest_os_type          = "ubuntu"
  http_directory         = "${local.http_directory}"
  iso_checksum           = "${var.iso_checksum}"
  iso_url                = "${var.mirror}/${var.mirror_directory}/${var.iso_name}"
  memory                 = "1024"
  output_directory       = "${var.build_directory}/packer-${var.basebox_name}-parallels"
  parallels_tools_flavor = "lin-arm"
  prlctl_version_file    = ".prlctl_version"
  shutdown_command       = "echo 'vagrant' | sudo -S shutdown -P now"
  ssh_password           = "vagrant"
  ssh_port               = 22
  ssh_timeout            = "10000s"
  ssh_username           = "vagrant"
  vm_name                = "${var.basebox_name}"
}

build {
  sources = ["source.parallels-iso.ubuntu-22-04-arm64"]

  provisioner "shell" {
    environment_vars  = ["HOME_DIR=/home/vagrant"]
    execute_command   = "echo 'vagrant' | {{ .Vars }} sudo -S -E sh -eux '{{ .Path }}'"
    expect_disconnect = true
    scripts = [
      "${path.root}/scripts/update.sh",
      "${path.root}/../_common/motd.sh",
      "${path.root}/../_common/sshd.sh",
      "${path.root}/scripts/networking.sh",
      "${path.root}/scripts/sudoers.sh",
      "${path.root}/../_common/vagrant.sh",
      "${path.root}/../_common/parallels.sh",
      "${path.root}/scripts/cleanup.sh",
      "${path.root}/../_common/minimize.sh"
    ]
  }

  post-processor "vagrant" {
    output = "${var.build_directory}/${var.basebox_name}.box"
  }
}
