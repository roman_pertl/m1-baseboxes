packer {
  required_plugins {
    parallels = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/parallels"
    }
  }
}

variables {
  basebox_name     = "ubuntu-20.04-arm64"
  build_directory  = "../builds"
  iso_name         = "ubuntu-20.04.5-live-server-arm64.iso"
  iso_checksum     = "e42d6373dd39173094af5c26cbf2497770426f42049f8b9ea3e60ce35bebdedf"
  mirror           = "http://cdimage.ubuntu.com"
  mirror_directory = "releases/20.04/release"
}

locals {
  http_directory = "${path.root}/http"
}

source "parallels-iso" "ubuntu-20-04-arm64" {
  boot_command           = ["<esc>", "linux /casper/vmlinuz", " quiet", " autoinstall", " ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/'", "<enter>", "initrd /casper/initrd<enter>", "boot<enter>"]
  boot_wait              = "5s"
  cpus                   = "2"
  disk_size              = "65536"
  guest_os_type          = "ubuntu"
  http_directory         = "${local.http_directory}"
  iso_checksum           = "${var.iso_checksum}"
  iso_url                = "${var.mirror}/${var.mirror_directory}/${var.iso_name}"
  memory                 = "1024"
  output_directory       = "${var.build_directory}/packer-${var.basebox_name}-parallels"
  parallels_tools_flavor = "lin-arm"
  prlctl_version_file    = ".prlctl_version"
  shutdown_command       = "echo 'vagrant' | sudo -S shutdown -P now"
  ssh_password           = "vagrant"
  ssh_port               = 22
  ssh_timeout            = "10000s"
  ssh_username           = "vagrant"
  vm_name                = "${var.basebox_name}"
}

build {
  sources = ["source.parallels-iso.ubuntu-20-04-arm64"]

  provisioner "shell" {
    environment_vars  = ["HOME_DIR=/home/vagrant"]
    execute_command   = "echo 'vagrant' | {{ .Vars }} sudo -S -E sh -eux '{{ .Path }}'"
    expect_disconnect = true
    scripts = [
      "${path.root}/scripts/update.sh",
      "${path.root}/../_common/motd.sh",
      "${path.root}/../_common/sshd.sh",
      "${path.root}/scripts/networking.sh",
      "${path.root}/scripts/sudoers.sh",
      "${path.root}/../_common/vagrant.sh",
      "${path.root}/../_common/parallels.sh",
      "${path.root}/scripts/cleanup.sh",
      "${path.root}/../_common/minimize.sh"
    ]
  }

  post-processor "vagrant" {
    output = "${var.build_directory}/${var.basebox_name}.box"
  }
}
