#!/bin/sh -eux

# set a timezone
#echo 'vagrant' | sudo -S timedatectl set-timezone America/Los_Angeles
timedatectl set-timezone America/Los_Angeles

# Install dev tools
apt-get install -y  gdb pkg-config clang-11 lldb-11   \
                    lld-11 valgrind git python3-pip   \
                    libglib2.0-dev gcc-arm-none-eabi  \
                    gdb-arm-none-eabi meson           \
                    picolibc-arm-none-eabi 
