#!/bin/sh -eux

m1baseboxes='
m1-baseboxes inspired by the packer templates in the Bento project at https://github.com/chef/bento
Source and documentation for m1-baseboxes resides at https://gitlab.com/jnh3/m1-baseboxes'

if [ -d /etc/update-motd.d ]; then
    MOTD_CONFIG='/etc/update-motd.d/99-m1-baseboxes'

    cat >> "$MOTD_CONFIG" <<M1BASEBOXES
#!/bin/sh

cat <<'EOF'
$m1baseboxes
EOF
M1BASEBOXES

    chmod 0755 "$MOTD_CONFIG"
else
    echo "$m1baseboxes" >> /etc/motd
fi
